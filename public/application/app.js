'user strict';
function startConverse()
{

	converse.initialize({
		bosh_service_url: 'http://fire.nexos.im/http-bind', // Please use this connection manager only for testing purposes
		//websocket_url: 'http://fire.nexos.im:7070/ws', // Please use this connection manager only for testing purposes
		i18n: locales.es, // Refer to ./locale/locales.js to see which locales are supported
		show_controlbox_by_default: true,
		auto_login: true,
		jid: appConfig.jid + '@fire.nexos.im',
		password: "123456",
		auto_list_rooms: true,
		roster_groups: true
	});
	converse.listen.on('initialized', converseInit);
	converse.listen.on('ready', converseReady);
}

var _chats = null;
function converseInit(event)
{
	console.log('Converse init');
}

function converseReady(event)
{
	_chats = converse.contacts.get();
}


var NexosApp = angular.module('NexosApp', ['ngRoute', 'ngResource']);

NexosApp.config(function($routeProvider){
	$routeProvider
		.when('/chats', { templateUrl:appConfig.views + '/chats'})
		.when('/chat/:chatID?', { templateUrl:appConfig.views + '/chat'})
		.when('/contactos/:segmentID?', { templateUrl:appConfig.views + '/contactos'})
		.when('/grupos',{ templateUrl:appConfig.views + '/grupos'})
		.when('/ajustes',{ templateUrl:appConfig.views + '/ajustes'})
		.otherwise({ redirectTo: '/chats' });

});

NexosApp.controller('ChatsController', function ($scope, UsersFactory, $interval) {
	$scope.chats = UsersFactory.chats;
	startConverse();

	$interval(updateChat, '1000');

	function updateChat()
	{
		if(_chats != null)
			$scope.chats = _chats;
	}

	$scope.updateList = function()
	{
	};


});

NexosApp.controller('StatusListController', function ($scope, UsersFactory, $interval) {
	$scope.users = {};
	$interval(updateChat, '1000');
	function updateChat()
	{
		console.log('interval');
		if(_chats != null)
			$scope.users.online = _chats;
	}
});


NexosApp.factory('UsersFactory', function()
{
	return {
		users:{
			online:[
				{'name': 'Alberto Tejada', 'area': 'Marketing'},
				{'name': 'Gerson', 'area': 'Recurso Humanos'},
				{'name': 'Otro nombre', 'area': 'Marketing'}
			],
			offline:[
				{'name': 'Roberto Gonzales', 'area': 'Marketing'},
				{'name': 'Pedro Martinez', 'area': 'Marketing'},
			]
		},
		chats:[
			{'fullname': 'Cargando...'},
		]
	}
});