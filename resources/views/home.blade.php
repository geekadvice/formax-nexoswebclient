@extends('app')

@section('content')
	<div>
		<div id="main-menu" class="column medium-2">
			<div class="avatar text-center content">
				<img src="{{ asset('images/avatar.png') }}" alt="{{ Auth::user()->name }}"/>
				<br/>
				<br/>
				{{ Auth::user()->name }}
			</div>
			<div class="inner-column">
				<ul>
					<li><a href="#chats"><i class="icon-bubbles main-menu-icon"></i><br/>Chats</a></li>
					<li><a href="#contactos"><i class="icon-notebook main-menu-icon"></i><br/>Contactos</a></li>
					<li><a href="#grupos"><i class="icon-users main-menu-icon"></i><br/>Grupos</a></li>
					<li><a href="#ajustes"><i class="icon-settings main-menu-icon"></i><br/>Ajustes</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="column medium-10">

			<div class="content-bar inner-column">
				<div class="column medium-7">
					<label for="search" class="inline-label">
						<i class="icon-search icon-magnifier"></i>
						<input class="inline" name="search" id="search" type="text" placeholder="Haz click para buscar"/>
					</label>
				</div>
				<div class="column medium-2 actions text-right">
					<a href="{{URL::to('auth/logout')}}"><i class="icon-bubble"></i></a>
					<a href="{{URL::to('auth/logout')}}"><i class="icon-users"></i></a>
					<a href="{{URL::to('auth/logout')}}"><i class="icon-bubbles"></i></a>

				</div>
				<div class="column medium-3 text-center">
					<a href="{{URL::to('auth/logout')}}">Cerrar sesión <i class="icon-power icon23"></i></a>
				</div>
			</div>

			<!-- End top bar -->

			<div class="content-section inner-column">
				<div class="column medium-9">
					<div ng-view></div>
				</div>

				<!-- Lista de usuarios conectados -->
				<div class="column medium-3" ng-controller="StatusListController">
					<div class="inner-column">
						<div class="block-contacts">
							<div class="block-online-head">Conectados</div>
							<ul>
								<li ng-repeat="u in users.online">
									<a href="#contacto"><img src="<?php echo asset('images/avatar.png'); ?>" width="45" alt="<?php echo Auth::user()->name; ?>"/>@{{u.fullname}} <br/> <span class="text-info">@{{u.chat_status}}</span></a>
								</li>
							</ul>
						</div>

						<div class="block-contacts">
							<div class="block-offline-head">Sin conexión</div>
							<ul>
								<li ng-repeat="u in users.offline">
									<a href="#contacto"><img src="<?php echo asset('images/avatar.png'); ?>" width="45" alt="<?php echo Auth::user()->name; ?>"/>@{{u.fullname}} <br/> <span class="text-info">@{{u.chat_status}}</span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
