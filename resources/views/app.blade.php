<!DOCTYPE html>
<html lang="en" ng-app="NexosApp">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Nexos :. {{ $title or 'Bienvenido' }}</title>
	<link href="{{ asset('output/all_app.css') }}" rel="stylesheet">
	<link href="{{ asset('icons.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	<![endif]-->

	<script>
		var appConfig = {
			@if(Auth::check())
			jid:"{{ Auth::user()->jid }}",
			@endif
			views:"<?php echo URL::to('home'); ?>",
			root:"<?php echo URL::to(''); ?>"
		}
	</script>
	<script src="{{ asset('output/all_app.js') }}"></script>
	<link href="{{ asset('application/converse/converse.min.css') }}" rel="stylesheet">
	<script src="{{ asset('application/converse/locales.js') }}"></script>
	<script src="{{ asset('application/converse/templates.js') }}"></script>
	<script src="{{ asset('application/converse/converse.min.js') }}"></script>

</head>
<body>
	@yield('content')


	@yield('scripts')
</body>
</html>
