@extends('app')

@section('content')
	<div id="candy"></div>
	<div class="column small-centered large-4 medium-5 text-center">
		<h3>Nexos</h3>

		<div class="block-form">
			<div class="block-form-logo">
				<object type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}">
					<img src="{{ asset('svg_sprites/logo.png') }}" alt="">
				</object>
			</div>
			<div class="content">
				<form role="form" method="POST" action="{{ url('/auth/login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					@if (count($errors) > 0)
						<div class="alert-box">
							<strong>Informacion!</strong>
							@foreach ($errors->all() as $error)
								<li style="color: #ffffff">{{ $error }}</li>
							@endforeach
						</div>
					@endif

					<input type="email" name="email" placeholder="Ingresa tu correo electrónico"
						   value="{{ old('email') }}">
					<input type="password" name="password" placeholder="Ingresa tu contraseña">

					<p>
						<button type="submit" class="small">Ingresar</button>
					</p>
					<input id="member" type="checkbox" name="remember">
					<label for="member">Recordame</label>
				</form>
			</div>
		</div>
		<p><a href="{{ url('/password/email') }}">No recuerdo mi contraseña</a></p>
	</div>
@endsection
