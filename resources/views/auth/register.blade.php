@extends('app')

@section('content')
	<div class="column small-centered large-4 medium-5">
		<h3 class="text-center">Nexos</h3>

		<div class="block-form">

			<div class="block-form-logo">
				<object type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}">
					<img src="{{ asset('svg_sprites/logo.png') }}" alt="">
				</object>
			</div>
			<div class="content">

				@if (count($errors) > 0)
					<div class="alert-box">
						<strong>Whoops!</strong> Encontramos algunos problemas en tu registro.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<label class="col-md-4 control-label">Nombres y Apellidos</label>

						<div class="col-md-6">
							<input type="text" class="form-control" name="name" value="{{ old('name') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">E-Mail</label>

						<div class="col-md-6">
							<input type="email" class="form-control" name="email" value="{{ old('email') }}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Contraseña</label>

						<div class="col-md-6">
							<input type="password" class="form-control" name="password">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">Confirma tu contraseña</label>

						<div class="col-md-6">
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>

					<div class="text-center">
						<button type="submit" class="btn btn-primary">Registrarme</button>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection
