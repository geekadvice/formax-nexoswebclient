<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::table('users')->delete();

		$rAdmin = \App\Role::create(['name' => 'admin', 'display_name' => 'Administrador', 'description' => 'Administrador de sistema']);
		\App\Role::create(['name' => 'editor', 'display_name' => 'Editor', 'description' => 'Creación de contenidos']);
		\App\Role::create(['name' => 'user', 'display_name' => 'Usuario', 'description' => 'Usuario del sistema']);

		$admin = \App\User::create([
			'name' => 'Gerson',
			'jid' => 'gerson',
			'email' => 'gerson@geekadvice.pe',
			'password' => \Illuminate\Support\Facades\Hash::make('123456')
		]);

		\App\User::create([
			'name' => 'Rolando',
			'jid' => 'rolando',
			'email' => 'rpezan@formax.pe',
			'password' => \Illuminate\Support\Facades\Hash::make('123456')
		]);
		\App\User::create([
			'name' => 'Phillip',
			'jid' => 'phillip',
			'email' => 'philip.reiser88@gmail.com',
			'password' => \Illuminate\Support\Facades\Hash::make('123456')
		]);

		\App\User::create([
			'name' => 'Renzo',
			'jid' => 'renzo',
			'email' => 'rcastillo@formax.pe',
			'password' => \Illuminate\Support\Facades\Hash::make('123456')
		]);

		$admin->attachRole($rAdmin);
		// $this->call('UserTableSeeder');
	}

}
