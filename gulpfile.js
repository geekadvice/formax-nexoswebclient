var elixir = require('laravel-elixir');
var gulp = require('gulp');
var gulpBowerFiles = require('main-bower-files');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.extend('bower', function(mix) {
	return gulp.src(gulpBowerFiles()).pipe(gulp.dest('public/libs'));
});


//----------------------------------------------------------------------- Application
elixir.extend('application', function(mix) {
	mix.styles([
		'libs/foundation.css',
		'css/app.css'
	], 'public/output/all_app.css', 'public/')
		.scripts([
			'libs/angular.js',
			'components/angular-foundation/mm-foundation.js',
			'components/angular-foundation/mm-foundation-tpls.js',
			'libs/angular-resource.js',
			'libs/angular-route.js',
			'libs/fastclick.js',
			'libs/TweenMax.js',
			//'libs/strophe.js',
			'libs/jquery.js',
			'application/app.js',
			'application/services.js',
			'application/controllers.js'
		], 'public/output/all_app.js', 'public/');
});


//----------------------------------------------------------------------- Website
elixir.extend('website', function(mix) {
	mix.styles([
		'libs/foundation.css',
		'css/site.css'
	], 'public/output/all.css', 'public')
		.scripts([
			'libs/jquery.js',
			'libs/foundation.js',
			'application/site.js'
		], 'public/output/all.js', 'public');
});

elixir(function(mix) {
	mix.less(['app.less']);
	mix.bower(mix);
	mix.application(mix);
	//mix.website(mix);
});